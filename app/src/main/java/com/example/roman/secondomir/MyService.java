package com.example.roman.secondomir;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

public class MyService extends Service {
    long start;
    boolean isTiming;

    private Binder binder = new ServiceBinder();
    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
      return binder;
    }
    public void setStart(long start) {
        this.start = start;
    }
    public long getStart(){return start;}

    public boolean isTiming() {
        return isTiming;
    }

    public void setTiming(boolean timing) {
        this.isTiming = timing;
    }

    public void foreground(){startForeground(1,createNotif().build());}

    private NotificationCompat.Builder createNotif(){
        PendingIntent pendingIntent = PendingIntent.getActivity(this , 1 , new Intent(this , MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
        return new NotificationCompat.Builder(this)
                .setTicker("0000000")
                .setContentTitle("Stop Watch")
                .setContentText("--------")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setUsesChronometer(false)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);
    }



    class ServiceBinder extends Binder{

        MyService getService(){
            return MyService.this;
        }

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_NOT_STICKY;
    }
}
