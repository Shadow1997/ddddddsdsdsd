package com.example.roman.secondomir;


import android.os.Handler;
import android.os.Message;
import java.lang.ref.WeakReference;

public class TimerHandler extends Handler {
    private final WeakReference<MainActivity> reference;

    TimerHandler(MainActivity activity) {
        reference = new WeakReference(activity);
    }

    @Override
    public void handleMessage(Message msg) {
        reference.get().showTime();
        sendEmptyMessageDelayed(0, 100);
    }
}

