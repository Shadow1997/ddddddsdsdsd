package com.example.roman.secondomir;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private final Handler handler = new TimerHandler(this);
    private TextView textView, mText, textView2, textView3;
    private EditText mE1;
    long lastDown;
    private boolean flag = false;
    private String ed_text;
    long lastDuration;
    private String convert;
    private MyService serv;
    private String time;
    AlertDialog.Builder ad;
    Context context;

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MyService.ServiceBinder binder = (MyService.ServiceBinder) service;
            serv = binder.getService();

            if (serv.isTiming())
                startUpdateUI();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            serv = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView2 = findViewById(R.id.TextView2);
        textView = findViewById(R.id.TextView1);
        textView3 = findViewById(R.id.TextView3);
        Button mBOk = findViewById(R.id.buttonok);
        Button mB1 = findViewById(R.id.button1);
        Button mB2 = findViewById(R.id.button2);
        Button mB3 = findViewById(R.id.button3);
        Button mB4 = findViewById(R.id.button4);
        Button mB5 = findViewById(R.id.button5);
        Button mB6 = findViewById(R.id.button6);
        Button mB7 = findViewById(R.id.button7);
        Button mB8 = findViewById(R.id.button8);
        Button mB9 = findViewById(R.id.button9);
        Button mB0 = findViewById(R.id.button0);
        Button mBC = findViewById(R.id.buttonC);
        Button mBCE = findViewById(R.id.buttonCE);
        mBOk.setOnClickListener(this);
        mB0.setOnClickListener(this);
        mB2.setOnClickListener(this);
        mB1.setOnClickListener(this);
        mB3.setOnClickListener(this);
        mB4.setOnClickListener(this);
        mB5.setOnClickListener(this);
        mB6.setOnClickListener(this);
        mB7.setOnClickListener(this);
        mB8.setOnClickListener(this);
        mB9.setOnClickListener(this);
        mBC.setOnClickListener(this);
        mBCE.setOnClickListener(this);
        mText = findViewById(R.id.TextView);
        mE1 = findViewById(R.id.editText);
        mE1.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int length = mE1.length();
                convert = String.valueOf(length);
                ed_text = mE1.getText().toString().trim();
                if (ed_text.isEmpty() || ed_text.length() == 0 || ed_text.equals("") || ed_text == null) {

                } else {
                    if (serv != null) {
                        serv.setStart(System.currentTimeMillis());
                        if (!serv.isTiming()) {
                            startUpdateUI();
                            serv.setTiming(true);
                        }
                    }
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        mB2.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    lastDown = System.currentTimeMillis() - serv.getStart();
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    lastDuration = System.currentTimeMillis() - serv.getStart() - lastDown;
                }
                textView2.setText(String.valueOf(lastDuration));
                return false;

            }
        });
        mB3.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    lastDown = System.currentTimeMillis() - serv.getStart();
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    lastDuration = System.currentTimeMillis() - serv.getStart() - lastDown;
                }
                textView2.setText(String.valueOf(lastDuration));
                return false;

            }
        });
        mB4.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    lastDown = System.currentTimeMillis() - serv.getStart();
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    lastDuration = System.currentTimeMillis() - serv.getStart() - lastDown;
                }
                textView2.setText(String.valueOf(lastDuration));
                return false;

            }
        });
        mB5.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    lastDown = System.currentTimeMillis() - serv.getStart();
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    lastDuration = System.currentTimeMillis() - serv.getStart() - lastDown;
                }
                textView2.setText(String.valueOf(lastDuration));
                return false;

            }
        });
        mB6.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    lastDown = System.currentTimeMillis() - serv.getStart();
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    lastDuration = System.currentTimeMillis() - serv.getStart() - lastDown;
                }
                textView2.setText(String.valueOf(lastDuration));
                return false;

            }
        });
        mB7.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    lastDown = System.currentTimeMillis() - serv.getStart();
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    lastDuration = System.currentTimeMillis() - serv.getStart() - lastDown;
                }
                textView2.setText(String.valueOf(lastDuration));
                return false;

            }
        });
        mB8.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    lastDown = System.currentTimeMillis() - serv.getStart();
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    lastDuration = System.currentTimeMillis() - serv.getStart() - lastDown;
                }
                textView2.setText(String.valueOf(lastDuration));
                return false;

            }
        });
        mB9.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    lastDown = System.currentTimeMillis() - serv.getStart();
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    lastDuration = System.currentTimeMillis() - serv.getStart() - lastDown;
                }
                textView2.setText(String.valueOf(lastDuration));
                return false;

            }
        });
        mB1.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    lastDown = System.currentTimeMillis() - serv.getStart();
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    lastDuration = System.currentTimeMillis() - serv.getStart() - lastDown;
                }
                textView2.setText(String.valueOf(lastDuration));
                return false;

            }
        });
        mB0.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    lastDown = System.currentTimeMillis() - serv.getStart();
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    lastDuration = System.currentTimeMillis() - serv.getStart() - lastDown;
                }
                textView2.setText(String.valueOf(lastDuration));
                return false;

            }
        });
        mBOk.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    lastDown = System.currentTimeMillis() - serv.getStart();
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    lastDuration = System.currentTimeMillis() - serv.getStart() - lastDown;
                }
                textView2.setText(String.valueOf(lastDuration));
                return false;

            }
        });

    }

    public void showTime() {
        if (serv != null)
            time = String.valueOf((System.currentTimeMillis() - serv.getStart()));
            textView.setText(time);
    }

    private void startUpdateUI() {
        handler.sendEmptyMessage(0);
    }

    private void stopUpdateUi() {
        handler.removeMessages(0);
    }

    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(MainActivity.this, MyService.class);
        startService(intent);
        bindService(intent, connection, 0);
    }

    protected void onStop() {
        super.onStop();
        if (serv != null)
            unbindService(connection);
    }

    protected void onDestroy() {
        super.onDestroy();
        if (serv != null)
            serv.foreground();
    }

    public void onBackPressed() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Ви, впевнені?");
        builder.setCancelable(true);
        builder.setNegativeButton("Ні", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
            }
        });
        builder.setPositiveButton("Закрити!!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                finish();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();


    }

    @Override
    public void onClick(View v) {
        if (!flag) {
            if (serv != null) {
                serv.setStart(System.currentTimeMillis());
                if (!serv.isTiming()) {
                    startUpdateUI();
                    serv.setTiming(true);
                    flag = true;
                }
            }
        }
        switch (v.getId()) {
            case R.id.button0: {
                mE1.setText(mE1.getText() + "0");
            }
            break;
            case R.id.button1: {
                mE1.setText(mE1.getText() + "1");
            }
            break;
            case R.id.button2: {
                mE1.setText(mE1.getText() + "2");
            }
            break;
            case R.id.button3: {
                mE1.setText(mE1.getText() + "3");
            }
            break;
            case R.id.button4: {
                mE1.setText(mE1.getText() + "4");
            }
            break;
            case R.id.button5: {
                mE1.setText(mE1.getText() + "5");
            }
            break;
            case R.id.button6: {
                mE1.setText(mE1.getText() + "6");
            }
            break;
            case R.id.button7: {
                mE1.setText(mE1.getText() + "7");
            }
            break;
            case R.id.button8: {
                mE1.setText(mE1.getText() + "8");
            }
            break;
            case R.id.button9: {
                mE1.setText(mE1.getText() + "9");
            }
            break;
            case R.id.buttonok: {
                stopUpdateUi();
                flag = false;
                serv.setTiming(false);
                double a = Double.parseDouble(convert.toString());
                double b = Double.parseDouble(String.valueOf((System.currentTimeMillis() - serv.getStart())));
                double res = a / b;
                textView3.setText(String.valueOf(res));

            }
            break;
            case R.id.buttonC: {
                mE1.setText(" ");
            }
            break;
            case R.id.buttonCE: {
                try {
                    StringBuilder stb = new StringBuilder(mE1.getText());
                    stb.deleteCharAt(mE1.getText().length() - 1);
                    mE1.setText(stb.toString());
                    break;
                } catch (Exception e) {
                    Toast.makeText(this, "Error", Toast.LENGTH_LONG).show();
                }
            }


        }
    }
}

